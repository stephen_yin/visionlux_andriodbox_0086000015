	$(function() {

		var url = "admin/server/column.php?port=getlist&lang=" + LANG;

		$.getJSON(url, function(json_obj) {

			if(json_obj.ret == 1) {
				var substr = "";
				var j = 0;
				var l = 0;
				var h = 0;
				var mid = "0";
				for(var i = 0; i < json_obj.data.length; i++) {

					if(json_obj.data[i].pid == mid && json_obj.data[i].cate_status != (-1)) {
						j++;

						$('.bgbox p').eq(h).html(json_obj.data[i].cate_name);

						substr += '<ul id="category-menu-' + j + '">';
						for(var k = 0; k < json_obj.data.length; k++) {
							if(json_obj.data[k].pid == json_obj.data[i].id && json_obj.data[k].cate_status != (-1)) {

								substr += '<li><a  href="javascript:" onclick="gogo(this);"  id=' + json_obj.data[k].cate_link + '>' + json_obj.data[k].cate_name + '</a></li>';
							}
						}
						substr += '</ul>';
						h++;
					}
				}

				$(".top_main").html(substr);
				/**
				 * 初始化遥控器焦点；
				 */
				$(".top_main ul").eq(focusId).addClass('cur').siblings().removeClass('cur');
				$(".bot_main>div").eq(focusId).addClass('cur').siblings().removeClass('cur');
				$(".top_main ul li:last-child a").eq(focusId).focus();
			}
		});

		if(!isPC()) {
			if(LANG == "cn") {
				JS.goTOMain(1);
			} else if(LANG == "en") {
				JS.goTOMain(2);
			} else {
				JS.goTOMain(2);
			}
		}
		//	导航条时间接口部分
		var url = "admin/server/clock.php?port=getinfo";
		$.getJSON(url, function(json_obj) {
			var str1 = '';
			if(LANG == 'en') {
				str1 += '<p class="time" id="time">' + json_obj.data.time1 + '</p><br />';
				str1 += '<p class="data" id="data">';
				str1 += '<span>' + json_obj.data.date1 + '</span>';
				str1 += ' <span>' + json_obj.data.week_en + '</span>';
				str1 += '</p>';
			} else {
				str1 += '<p class="time" id="time">' + json_obj.data.time1 + '</p><br />';
				str1 += '<p class="data" id="data">';
				str1 += '<span>' + json_obj.data.date2 + '</span>';
				str1 += ' <span>' + json_obj.data.week_cn + '</span>';
				str1 += '</p>';
			}
			$(".hdright div").html(str1);
		});

	});

	//导航条天气接口部分
	var url = "admin/server/weather.php?port=getlist";
	var weather_imgurl = 'images/wicon/';
	$.getJSON(url, function(json_obj) {

		$(".weather span").html(json_obj.data.result[0].temperature);
		$(".weather img").attr('src', weather_imgurl + json_obj.data.result[0].weatid + '.png');
	});

	//消息接口			
	var url = "admin/server/mpi.php?port=getlist" + "&lang=" + LANG
	$.getJSON(url, function(json_obj) {

		var ver = 0;
		if(ver < json_obj.data.version) {

		}
		var str0 = '';
		for(var m = 0; m < json_obj.data.message.length; m++) {

			str0 += '<li>' + json_obj.data.message[m].mpi_msg + '</li>';

		}

		$(".apple ul").append(str0);

		setInterval('autoScroll(".apple")', 10000);
	});

	function autoScroll(obj) {
		$(obj).find("ul").animate({
			marginTop: "-39px"
		}, 500, function() {
			$(this).css({
				marginTop: "0px"
			}).find("li:first").appendTo(this);
		})
	}

	function gogo(me) {
		var focusId2 = getCookie('focusId');
		var mainName = encodeURIComponent($(".bot_main>div p").eq(focusId2).html());
		var subName = encodeURIComponent($(me).html());
		var linkUrl = $(me).attr("id");
		if(!isPC()) {
			JS.leaveMain();

		}

		if(linkUrl == "worldtime.html" || linkUrl == "weather.html" || linkUrl == "money_rate.html" || linkUrl == "hdtv.html") {
			window.location.href = linkUrl + "?lang=" + LANG + "&ver=" + Date.parse(new Date());

		} else if(linkUrl.indexOf('JS') > 0) {
			window.location.href = linkUrl;
		} else {
			window.location.href = linkUrl + '&mainName=' + mainName + '&subName=' + subName + "&lang=" + LANG + "&ver=" + Date.parse(new Date());
		}
	}