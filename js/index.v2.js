
	/**
	 * 获取homepage
	 */

	function getHomePage() {
		var requestUrl = localStorage.apiServer + "/page/butler_homepage/?" + $.param(BASEDATA);
		//获取主页接口的数据
		$.ajax(requestUrl).then(
			function (json) {
				console.log("butler_homepage:", json);
				//不符合规则的数据进行提示
				if(json.data.homepage.blocks.length > 6 ){
					$(".bot_main").html("本系统暂不支持多于6个大分类!");
				}
				
				
				
				
				var pageId_arr = [];
				var small_imgurl_arr = [];
				var big_imgurl_arr = [];
				var IMGLIST = localStorage.imgList ? JSON.parse(localStorage.imgList) : {};


				//获取到首页左上角的LOGO
				var imageId = JSON.stringify(json.data.homepage.logo) != "{}" ? json.data.homepage.logo.internalImageId : null;
				var logo = typeof imageId !== null ? IMGLIST.data[imageId].internal_image_url : "images/logo.png";
				localStorage.logo = logo;
				$("img.lt").attr("src", logo);

				



				//获取分类的页面信息和图标图片(大小两张图,取第一张为小图.)(没有图片的分类不显示)
				$.each(json.data.homepage.blocks, function () {

					if ($(this)[0].hasOwnProperty("backgroundImages")) {
						if ($(this)[0].backgroundImages.length > 0) {
							var infopageId = $(this)[0].pageId; //页面ID信息
							//小图片ID信息
							var this_smallimgId = (typeof $(this)[0].backgroundImages[0] != 'undefined') ? $(this)[0].backgroundImages[0].internalImageId : null;
							//大图片ID信息
							var this_bigimgId = (typeof $(this)[0].backgroundImages[1] != 'undefined') ? $(this)[0].backgroundImages[1].internalImageId : null;

							pageId_arr.push(infopageId);
							if(this_smallimgId !=null)	small_imgurl_arr.push(IMGLIST.data[this_smallimgId].internal_image_url);
							if(this_bigimgId !=null) big_imgurl_arr.push(IMGLIST.data[this_bigimgId].internal_image_url);
						}
					}else{
						pageId_arr = [];
						small_imgurl_arr =[];
						big_imgurl_arr= [];
					}

				});
				// localStorage.big_imgurl_arr = JSON.stringify(big_imgurl_arr) ;
				if(pageId_arr.length>0 && small_imgurl_arr.length>0 && big_imgurl_arr.length>0){			
				
					getTitle(pageId_arr.join(","), small_imgurl_arr, big_imgurl_arr);//获取分类标题
				}else{
					$(".bot_main").html("后台数据不完整,请添加完整!");
				}
				// $(".apple ul").append(JSON.stringify(BASEDATA));
				delete BASEDATA.page_type;
				delete BASEDATA.lang_agnostic_id;
			},

			function (err) {
				// $(".apple ul").append(err.responseText);
				// $(".apple ul").append(JSON.stringify(BASEDATA));
				$(".apple ul").append("获取主页数据错误,请检查后台数据是否错误!");
				console.error("butler_homepage: " + err.responseText);
				if(isTizen()) location.href="tv.html";
			}
		);
	}


	function getTitle(pageId, small_imgurl_arr, big_imgurl_arr) {
		BASEDATA.page_id = pageId;
		var requestUrl = localStorage.apiServer + "/page/batch/?" + $.param(BASEDATA);
		$.ajax(requestUrl).then(function (json) {
			console.log(json);
			var mainMenu = '';
			var subMenu = '';
			var submenu_pageId = '';
			var licount = 0;
			for (var i = 0; i < json.data.length; i++) {

				mainMenu += '<div class="bgbox img' + i + '" onclick="showMenu($(this));">';
				mainMenu += '<div>';
				mainMenu += '<span class="small" style="background: url(' + small_imgurl_arr[i] + ') center center no-repeat;	"></span>';
				mainMenu += '<span class="big" style="background: url(' + big_imgurl_arr[i] + ') center center no-repeat;	background-size: cover;"></span>';
				mainMenu += '<p>' + json.data[i].translates[localStorage.defaultLanguage].title + '</p>';
				mainMenu += '</div>';
				mainMenu += '</div>';
				//预先加载子菜单框架
				subMenu += '<ul id="category-menu-' + i + '" data-main_pageid="' + json.data[i].id + '" data-sub_pageid="' + json.data[i].infoListIds.join(",") + '" class=""></ul>';
				licount++;
			}

			$(".bot_main").html(mainMenu);
			$(".top_main").html(subMenu);

			// 保存列表总数
		
			setCookie('licount',licount,1);

			//页面初始化焦点位置;
			var j=0;
			var focusId = getCookie('focusId'); //从cookie获取焦点ID值;
			console.log("focusid:",focusId)
			if(focusId != null){
				j = focusId;
			} else{
				setCookie('focusId',0,1);
			}

			$(".top_main ul").eq(j).addClass('cur').siblings().removeClass('cur');
			$(".bot_main>div").eq(j).addClass('cur').siblings().removeClass('cur');
			$(".top_main ul li:last-child a").eq(j).focus();
			//默认只加载当前焦点所在菜单的子菜单,加快页面显示速度.
			var sub_pageid = $('.top_main ul.cur').data("sub_pageid");
			getSubtitle(sub_pageid);

			//回收BASEDATA对象中的page_id键名;
			delete BASEDATA.page_id;
		}, function (err) {
			console.error(err);
		});
	}


	function getSubtitle(sub_pageid) {
		BASEDATA.page_id = sub_pageid;
		var requestUrl = localStorage.apiServer + "/page/batch/?" + $.param(BASEDATA);
		$.ajax(requestUrl).then(function (json) {
			console.log(sub_pageid, json);
			var subMenu = '';
			for (var i = 0; i < json.data.length; i++) {
				subMenu += '<li>';
				subMenu += '<a href="javascript:" onclick="gogo(this);" data-page="' + json.data[i].infoPageIds + '" data-lang_agnostic_id="' + json.data[i].langAgnosticId + '" data-type="' + json.data[i].subpage_type + '">' + json.data[i].translates[localStorage.defaultLanguage].title + '</a>';
				subMenu += '</li>';
			}
			$('.top_main ul.cur').html(subMenu);
			$(".top_main ul.cur li:last-child a").focus();
			console.log("chushiloadingindex：",$(".top_main ul.cur li:last-child").index())
			setCookie("subindex",$(".top_main ul.cur li:last-child").index(),1);


			//回收BASEDATA对象中的page_id键名;
			delete BASEDATA.page_id;
		}, function (err) {
			console.error(err.responseJSON);
		});

	}
	
	function getLinkinfo(linkId){
			BASEDATA.page_type = 'info_page';
			BASEDATA.lang_agnostic_id = linkId;
			BASEDATA.lang = defaultLanguage;
			var requestUrl = localStorage.apiServer + "/page/search/?" + $.param(BASEDATA);
			console.log('getWeather:', requestUrl);
			$.ajax(requestUrl).then(
			    function (json) {
			        console.log("getLink:", json);
					
					delete BASEDATA.page_type;
					delete BASEDATA.lang_agnostic_id;
					delete BASEDATA.lang;
			    },
			
			    function (err) {
			        console.error("getWeather: " + err.responseText);
			    }
			);
		
	}


	//确定键回调函数
	function gogo(me) {
		// console.log(me);
		try {

			//传入的参数
			var infoPageIds = $(me).data("page");
			var langAgnosticId = $(me).data("lang_agnostic_id");
			var mainMenuName = $('.bgbox.cur p').text();
			var subMenuName = $(me).text();

			//子页面类型
			var subPageType = $(me).data("type");
			var rand = Math.floor(Math.random()*(100000-20+1)+20);
			console.log(rand);

			//根据页面类型然后跳转到对应类型的页面;
			switch (subPageType) {

				case "INFO":
					$(me).html("载入中...");
					if(langAgnosticId=="shopinfo"){
						location.href = "detail_mall.html?ver="+rand+"&subMenuName=" + subMenuName + "&mainMenuName=" + mainMenuName + "&langAgnosticId=" + langAgnosticId + "&infoPageIds=" + infoPageIds;
					}else{
						location.href = "detail.html?ver="+rand+"&subMenuName=" + subMenuName + "&mainMenuName=" + mainMenuName + "&langAgnosticId=" + langAgnosticId + "&infoPageIds=" + infoPageIds;
					}
					
					break;

				case "MERCHANDISE_CATEGORY":
				case "MERCHANDISE":
					$(me).html("该功能暂未开放...");
					break;

				case "WEATHER":
					$(me).html("载入中...");
					location.href = "weather.html?ver="+rand+"&subMenuName=" + subMenuName + "&mainMenuName=" + mainMenuName;
					break;

				case "VOD":
					$(me).html("载入中...");
					location.href = "vodlist.html?ver="+rand+"&subMenuName=" + subMenuName + "&mainMenuName=" + mainMenuName + "&langAgnosticId=vod" + "&infoPageIds=" + infoPageIds;
					break;

				case "TV_CHANNEL":
						try{
							JS.tvPlay();
						}catch(e){
							if(isTizen()){
								location.href = "tv.html";
							}else if(isPC()){
								$(me).html("不支持PC端...");
								console.log("你的客户端是PC端,不支持该功能!")
							}else{
								$(me).html("未知设备");
							}
							
							
						}
				
						
				
						
					
					break;


				case "LINK":
					
					if (langAgnosticId == "clock") location.href = "worldtime.html?ver="+rand+"&";
					if(langAgnosticId == "多媒体"){
						try{
							JS.media();
						}catch(e){
							$(me).html("不支持PC端...");
							console.log("你的客户端是PC端,不支持该功能!")
						}						
					} 
					if (langAgnosticId == "multi_screen") {
						try{
							JS.lebo();
						}catch(e){
							$(me).html("不支持PC端...");
							console.log("你的客户端是PC端,不支持该功能!")
						}		
					}
					
					
					break;

				case "SERVICE":
					$(me).html("该功能暂未开放...");
					break;

				default:
					$(me).html("未知数据类型...");
					console.log('未知数据类型，或后台没有添加任何信息...');
					return false;
					break;
			}

		} catch (e) {
			//TODO handle the exception
			console.error(e.responseJSON);
			$(me).html("系统发生异常，请重启");
			return false;
		}
	}
