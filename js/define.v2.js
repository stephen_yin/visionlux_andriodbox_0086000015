var RUNMODE = "debug"; //debug调试开发模式 | online 有本地junction的情况下使用
var LANG = localStorage.defaultLanguage != null ? localStorage.defaultLanguage : "zh-cn"; //默认语言中文;(此处兼容1.0版参数)
var BASEDATA;
var pamrams;
(function () {

	$(".loadingDiv").show();
	/*自适应设备宽度 */
	var deviceWidth = parseInt(window.screen.width); //获取当前设备的屏幕宽度
	var deviceScale = deviceWidth / 1280; //得到当前设备屏幕与640之间的比例，之后我们就可以将网页宽度固定为640px
	var ua = navigator.userAgent;
	//获取当前设备类型（安卓或苹果）
	if (/Android (\d+\.\d+)/.test(ua)) {
		var version = parseFloat(RegExp.$1);
		console.log(version);
		
		if (version > 2.3) {
			document.write('<meta name="viewport" content="width=1280,initial-scale=' + deviceScale + ', minimum-scale = ' +
				deviceScale + ', maximum-scale = ' + deviceScale + ', target-densitydpi=device-dpi">');
		} else {
			document.write(
				'<meta name="viewport" content="width=1280,initial-scale=0.75,maximum-scale=0.75,minimum-scale=0.75,target-densitydpi=device-dpi" />'
			);
		}
	} else {
		document.write('<meta name="viewport" content="width=1280, user-scalable=no">');
	}
	

	/*配置获取接口的默认参数,硬件参数等基本数据 */
try {
	pamrams = JSON.parse(window.JS.getPamrams());
	BASEDATA = {
		hotel_id: pamrams.hotel_id,
		device_type: "tv",
		device_id: pamrams.uuid,
		restaurant_id: pamrams.restaurant_id
	}

} catch (error) {
	BASEDATA = {
		hotel_id: "0086000015",
		device_id: "84:62:23:24:25:a1",
		restaurant_id: "r001",
		device_type: "tv"
	}

}

/**
 * 接口服务器定义,调试模式默认调用云端junction; 
 */
localStorage.apiServer = (RUNMODE == "debug") ? "https://junction.dev.havensphere.com/api/junctioncore/v1" : "/api/junctioncore/v1";

//头部公共信息

//获取天气预报图标和温度
if( sessionStorage.temperature != 'undefined'){	

$(".weather img").attr("src", sessionStorage.weather_imgurl);
$(".weather span").text(sessionStorage.temperature);

}

//从本地缓存中读取LOGO
if (localStorage.logo !== undefined) {
	$("img.lt").attr("src", localStorage.logo);
}



/**
	 * 项目配置
	 */
	switch(BASEDATA.hotel_id){
		case "0086000012":
		
			if (localStorage.defaultLanguage == "zh-cn") {
				$(".bottom img").attr("src", "images/cn-blue.png");
			} else {
				$(".bottom img").attr("src", "images/en-blue.png");
			}
			$(".bottom").show();//显示底部导航提示图片
			loadCss('css/blue.css');//载入蓝色样式
		break;
		
		case "0086000015":
			if (localStorage.defaultLanguage == "zh-cn") {
				$(".bottom img").attr("src", "images/cn-white.png");
			} else {
				$(".bottom img").attr("src", "images/en-white.png");
			}
			$(".bottom").show();// 显示底部导航提示图片
			loadCss('css/white.css');//载入白色样式
			
		
		break;
		
		case "0086000017":
			if (localStorage.defaultLanguage == "zh-cn") {
				$(".bottom img").attr("src", "images/cn-white.png");
			} else {
				$(".bottom img").attr("src", "images/en-white.png");
			}
			$(".bottom").show();// 显示底部导航提示图片
			loadCss('css/0086000017.css');//载入白色样式
			localStorage.backgroudimg = "images/0086000017/bg.png"
			
			
		
		break;
		
		default:
		
			if (localStorage.defaultLanguage == "zh-cn") {
				$(".bottom img").attr("src", "images/cn-black.png");
			} else {
				$(".bottom img").attr("src", "images/en-black.png");
			}
			$(".bottom").hide();//隐藏底部导航提示图片
			loadCss('css/black.css');//载入黑色样式(默认样式)
		
		break;
	}
	

})();


window.onload = function(){
	$(".loadingDiv").hide();
}