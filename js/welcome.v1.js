/**
 * 原始对接PHP后台
 */

//		获取设备的MAC地址
if (!isPC()) {
    var MAC = JS.getMac();
    //			var MAC = '84:62:23:24:25:ED';
} else {
    var MAC = '84:62:23:24:25:ED';
}

/**
 * 以下为遥控器控制焦点代码；
 */
if (LANG == "en") {
    $("#en").focus();

} else {
    $("#cn").focus();
}

$("a").focus(function () {
    /**
     * 当前焦点增加指定样式，同时其他同类删除样式（a以外的标签可用);
     * $(this).addClass('on').siblings().removeClass('on'); 
     */

    var LANG = $(this).attr("id");
    var time = new Date();

    if (LANG == "en") {
        var url = "admin/server/welcome.php?port=getinfo&lang=en";
    } else {
        var url = "admin/server/welcome.php?port=getinfo&lang=cn";
    }


    //	接口部分
    $.getJSON(url, function (json_obj) {
        var str1 = '';
        str1 += json_obj.data.content;
        $('.content').html(str1);
    });
    var index = $(this).index();

    //             //	导航条时间接口部分
    var url = "admin/server/clock.php?port=getinfo";
    $.getJSON(url, function (json_obj) {
        var str0 = '';
        if (LANG == 'en') {
            str0 += '<p class="time" id="time">' + json_obj.data.time1 + '</p>';
            str0 += '<p class="data" id="data">' + json_obj.data.date1 + '  ' + json_obj.data.week_en +
                '</p>';
            $(".timedata").html(str0);
        } else {
            str0 += '<p class="time" id="time">' + json_obj.data.time1 + '</p>';
            str0 += '<p class="data" id="data">' + json_obj.data.date1 + '  ' + json_obj.data.week_cn +
                '</p>';
            $(".timedata").html(str0);
        }
    });

});







//		通过MAC地址获取设备的房间号		
var url = 'admin/server/facility.php?port=getinfo&tv_mac=' + MAC;
$.getJSON(url, function (json) {
    if (json.ret == 1) {
        var room_id = (json.data.room_id).split(".")[0];

        //		获取贵宾姓名信息的接口对接
        var url = "admin/server/guest.php?port=getinfo&room_id=" + room_id;
        $.getJSON(url, function (json) {
            if (json.ret == 1) {
                $('.gust_name').html(json.data.guest_name);
            }
        });
    }
});

//欢迎页内容接口部分
var url = "admin/server/welcome.php?port=getinfo&lang=" + LANG;
$.getJSON(url, function (json_obj) {
    console.log(json_obj);
    var str1 = '';
    str1 += json_obj.data.content;
    $('.content').html(str1);
});

var boss = $(".boss").html();
var content = $('.content').html();
var data = $("#data").html();


//	导航条天气接口部分
var url = "admin/server/weather.php?port=getlist";
var weather_imgurl = 'images/wicon/';
$(".weather img").hide();
$.getJSON(url, function (json_obj) {
    $(".weather span").html(json_obj.data.result[0].temperature);
    $(".weather img").attr('src', weather_imgurl + json_obj.data.result[0].weatid + '.png');
    $(".weather img").fadeIn();
})